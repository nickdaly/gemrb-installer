#!/bin/sh
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Extra Planescape: Torment (4CD version) content (so CDs aren't needed)

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

CD2="99be7c8544016302c820088091c52fef"
CD3="6ffd2c51a41603c7dca6196d9bef2e26"
CD4="b70322aaa7b1d61a168a766eba399085"

getcd 2  $CD2 cd2/movies2.bif
copylower "${CDMOUNT}/cd2/" "$TARGETDIR" || die

getcd 3 $CD3 cd3/AR0500.bif
copylower "${CDMOUNT}/cd3/" "$TARGETDIR" || die

getcd 4 $CD4 cd4/movies4.bif
copylower "${CDMOUNT}/cd4/" "$TARGETDIR" || die

teardown "$TARGETDIR"
