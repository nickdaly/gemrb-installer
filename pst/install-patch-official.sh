#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Planescape: Torment official patch v1.1
#
# info : http://www.planetbaldursgate.com/pst/media/files/patches/tormentreadme11.txt
# size : 3.4 MB
# date : 1/28/00

patch_url="http://www.sorcerers.net/Games2/Torment/Trmt11.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/patch-includes.sh"

parseargs "$@"

checkforbin cabextract unshield wget

setuptmp

getpatch $patch_url

cabextract -L "${TMPDIR}/${patch_name}" -d "$TMPDIR" || diesoftly
# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TMPDIR" -L x "${TMPDIR}/data1.cab" || diesoftly
cd "${TMPDIR}" && unshield -d . -L x "${TMPDIR}/data1.cab" || diesoftly

copylower "${TMPDIR}/program_executable_files/" "$TARGETDIR" || diesoftly

teardown "$TARGETDIR"
