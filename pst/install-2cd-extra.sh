#!/bin/sh
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Extra Planescape: Torment (2CD version) content (so CDs aren't needed)

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

CD2="610d9f6354be741b09fc25b5e9645328"

getcd 1 "$PST_2CD_MD5" data1.cab

# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TARGETDIR" -g OtherData -L x "${CDMOUNT}/data1.cab" || die
#unshield -d "$TARGETDIR" -g OtherData -L x "${CDMOUNT}/data2.cab" || die
cd "$TARGETDIR" && unshield -d . -g OtherData -L x "${CDMOUNT}/data1.cab" || die
cd "$TARGETDIR" && unshield -d . -g OtherData -L x "${CDMOUNT}/data2.cab" || die

mv "${TARGETDIR}/otherdata/"* "$TARGETDIR" || die
rmdir "${TARGETDIR}/otherdata" || die

getcd 2 "$CD2" "cd2/movies4.bif"

copylower "${CDMOUNT}/cd2/" "$TARGETDIR" || die

teardown "$TARGETDIR"
