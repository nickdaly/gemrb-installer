#!/bin/bash
# Copyright 2009 Tasos Latsas
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Icewind Dale (2CD version) extra content

scriptdir="$(dirname $0)"/..
. "$scriptdir/includes/generic-includes.sh"
. "$scriptdir/includes/cd-includes.sh"

parseargs "$@"

CD2="2356e87971437f78bce8164b1dc066fa"

getcd 2 $CD2 AUTORUN.DAT

copylower "${CDMOUNT}/CD2/DATA/" "${TARGETDIR}/data" || die

teardown "$TARGETDIR"
