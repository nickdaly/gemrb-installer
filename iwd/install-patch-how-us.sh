#!/bin/bash
# Copyright 2009 Tasos Latsas, Nick Daly
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Icewind Dale - Heart of Winter patch v1.42 - Trials of the Loremaster
#
# info : http://www.planetbaldursgate.com/iwd/media/files/patches/iwdreadme106.txt
# size : 69.4 MB
# date : ????

patch_url="http://www.sorcerers.net/Games2/IWD/how142eng.rar"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "$scriptdir/includes/generic-includes.sh"
. "$scriptdir/includes/patch-includes.sh"

parseargs "$@"

checkforbin cabextract wget

setuptmp

getpatch $patch_url

cabextract -L -s "${TMPDIR}/${patch_name}" -d "$TMPDIR" || diesoftly
unshield x -L "${TMPDIR}/disk1/data1.cab" -d "$TMPDIR"
LONGTMP="${TMPDIR}/minimum_compressed"
cp -R "${LONGTMP}/data/"* "${TARGETDIR}/data" || diesoftly
cp -R "${LONGTMP}/override/"* "${TARGETDIR}/override" || diesoftly
cp -R "${LONGTMP}_(us_english)/"* "${TARGETDIR}" || diesoftly
cp -R "${TMPDIR}/minimum_uncompressed_(us_english)/"* "${TARGETDIR}" || diesoftly

teardown "$TARGETDIR"
