#!/bin/bash
# Copyright 2009 Tasos Latsas
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Icewind Dale patch v1.06
#
# info : http://www.planetbaldursgate.com/iwd/media/files/patches/iwdreadme106.txt
# size : 3.2 MB
# date : 7/30/00

patch_url="http://www.sorcerers.net/Games2/IWD/iwdpatch106.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "$scriptdir/includes/generic-includes.sh"
. "$scriptdir/includes/patch-includes.sh"

parseargs "$@"

checkforbin cabextract wget

setuptmp

getpatch $patch_url

cabextract -L -s "${TMPDIR}/${patch_name}" -d "$TMPDIR" || diesoftly
cp -R "${TMPDIR}/override/"* "${TARGETDIR}/override" || diesoftly
cp -R "${TMPDIR}/scripts/"* "${TARGETDIR}/scripts" || diesoftly

teardown "$TARGETDIR"
