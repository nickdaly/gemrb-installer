#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Baldur's Gate & Tales of the Sword Coast (3CD version) minimal

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

getcd 1 $BG1_TOTSC_3CD_US_MD5 data1.cab

mkdir -p "$TARGETDIR" || die

cp "${CDMOUNT}/dialog.tlk" "$TARGETDIR" || die

# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TARGETDIR" -L x "${CDMOUNT}/data1.cab" || die
cd "$TARGETDIR" && unshield -d . -L x "${CDMOUNT}/data1.cab" || die

move_and_remove "${TARGETDIR}/minimumdata" "${TARGETDIR}"
move_and_remove "${TARGETDIR}/english" "${TARGETDIR}"
move_and_remove "${TARGETDIR}/english_sounds/sounds" "${TARGETDIR}/sounds"
rm -r "${TARGETDIR}/english_sounds"

mkdir -p "${TARGETDIR}/movies"
mkdir -p "${TARGETDIR}/manual"

mkdir -p "${TARGETDIR}/save"
cp -R "${CDMOUNT}/"[Ss]ave/* "${TARGETDIR}/save" || die

cp "${CDMOUNT}/Manual/BGManual.pdf" "${TARGETDIR}/manual/" || die
cp "${CDMOUNT}/Manual/TotscManual.pdf" "${TARGETDIR}/manual/" || die

cp "${CDMOUNT}/baldur.ico" "$TARGETDIR" || die

teardown "$TARGETDIR"
