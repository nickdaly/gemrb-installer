#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# BG1/TOTSC Baldurdash fix pack v1.1
#
# info : http://www.baldurdash.org/BG1/bg1fixes.html
# size : 0.1 MB
# date : 7/07/01

patch_url="http://www.baldurdash.org/LargeDownloads/BG1FixPack11.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/patch-includes.sh"

parseargs "$@"

checkforbin unzip wget

setuptmp

getpatch $patch_url

# unzip complains unnecessarily about skipping non-zip
# part so don't print errors or die on failure
unzip -L "${TMPDIR}/${patch_name}" -d "$TMPDIR" 2>/dev/null
rm "${TMPDIR}/${patch_name}" "${TMPDIR}/bg1fixpack11readme.txt"
cp "${TMPDIR}/"* "${TARGETDIR}/override/" || diesoftly

teardown "$TARGETDIR"
