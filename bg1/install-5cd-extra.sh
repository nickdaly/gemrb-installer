#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Extra Baldur's Gate (5CD International version) content (so CDs aren't needed)

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

for CDNUMBER in 1 2 3 4 5; do
	moviesum=""
	case $CDNUMBER in
		2 ) moviesum="c04758a6af171bf9a35d8e3d4dcd807c" ;;
		3 ) moviesum="ba4224f50f890e10561d22f602d6a991" ;;
		4 ) moviesum="a066c263e13bf20b84d71c2ca7120fde" ;;
		5 ) moviesum="f19415703d3ccad01325b9338380ac7c" ;;
	esac

	if [ "$CDNUMBER" -gt 1 ]; then
		getcd "$CDNUMBER" "$moviesum" "CD${CDNUMBER}/Movies/MovieCD${CDNUMBER}.bif"
	fi

	copylower "${CDMOUNT}/cd${CDNUMBER}/" "$TARGETDIR"
done

teardown "$TARGETDIR"
