#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# BG1 official international patch v1.1.4315
#
# info : http://downloads.bioware.com/baldursgate1/bgint114315.txt
# size : 15 MB
# date : 2/17/99

patch_url="http://downloads.bioware.com/baldursgate1/bgintl114315.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/patch-includes.sh"

parseargs "$@"

checkforbin unzip unshield wget

setuptmp

getpatch $patch_url

unzip "${TMPDIR}/${patch_name}" -d "${TMPDIR}" || diesoftly
# work around unshield bug (#2801016 in SynCE project)
#unshield -g "$LANGUAGE" -d "$TMPDIR" -L x "${TMPDIR}/data1.cab" || diesoftly
cd "$TMPDIR" && unshield -g "$LANGUAGE" -d . -L x "${TMPDIR}/data1.cab" || diesoftly

LANGUAGE="$(echo $LANGUAGE|tr A-Z a-z|tr ' ' _)" # alter formatting
cp -R "${TMPDIR}/${LANGUAGE}/"* "$TARGETDIR" || diesoftly

teardown "$TARGETDIR"
