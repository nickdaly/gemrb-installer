#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# BG1 TOTSC official US/Canada patch v1.3.5512
#
# info : http://www.planetbaldursgate.com/bg/media/files/patches/TotSC5512Readme.txt
# size : 6.1 MB
# date : 8/3/99

patch_url="http://downloads.bioware.com/baldursgate1/BGTalesUS5512.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/patch-includes.sh"

parseargs "$@"

checkforbin cabextract unshield wget

setuptmp

getpatch $patch_url

cabextract -L "${TMPDIR}/${patch_name}" -d "$TMPDIR" || diesoftly
# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TMPDIR" -L x "${TMPDIR}/data1.cab" || diesoftly
cd "$TMPDIR" && unshield -d . -L x "${TMPDIR}/data1.cab" || diesoftly

cp -R "${TMPDIR}/minimumdata/"* "$TARGETDIR" || diesoftly

teardown "$TARGETDIR"
