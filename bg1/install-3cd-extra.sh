#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Extra Baldur's Gate (3CD version) content (so CDs aren't needed)

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

copylower "${CDMOUNT}/cd1/" "$TARGETDIR"

CD2_MD5="c04758a6af171bf9a35d8e3d4dcd807c"
CD3_MD5="ba4224f50f890e10561d22f602d6a991"

getcd 2 $CD2_MD5 CD2/Movies/MovieCD2.bif
copylower "${CDMOUNT}/cd2/" "$TARGETDIR"
copylower "${CDMOUNT}/cd5/" "$TARGETDIR"
cp "${CDMOUNT}/Manual/Baldur.pdf" "${TARGETDIR}/manual" || die

getcd 3 "$CD3_MD5" "CD3/Movies/MovieCD3.bif"
copylower "${CDMOUNT}/cd3/" "$TARGETDIR"
copylower "${CDMOUNT}/cd4/" "$TARGETDIR"
copylower "${CDMOUNT}/cd6/" "$TARGETDIR"

teardown "$TARGETDIR"
