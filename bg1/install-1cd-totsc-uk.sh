#!/bin/sh
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Tales of the Sword Coast (1CD UK version)

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

getcd 1 $BG1_TOTSC_1CD_UK_MD5 data1.cab

mkdir -p "$TARGETDIR" || die

# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TARGETDIR" -L x "${CDMOUNT}/data1.cab" || die
cd "$TARGETDIR" && unshield -d . -L x "${CDMOUNT}/data1.cab" || die

cp "${CDMOUNT}/dialog.tlk" "$TARGETDIR" || die

mkdir -p "${TARGETDIR/save}" || die

move_and_remove "${TARGETDIR}/minimumdata" "$TARGETDIR"
move_and_remove "${TARGETDIR}/english" "$TARGETDIR"

cp -R "${CDMOUNT}/override/"* "${TARGETDIR}/override/" || die

mkdir -p "${TARGETDIR}/movies"
copylower "$CDMOUNT/cd6/" "$TARGETDIR"

mkdir -p "${TARGETDIR}/mpsave"
cp -R "${CDMOUNT}/"[Ss]ave/* "${TARGETDIR}/mpsave" || die

teardown "$TARGETDIR"
