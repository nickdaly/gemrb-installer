# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.

### default variables ###

[ -z "$TMPDIR" ] && export TMPDIR="/tmp/iepatch"

[ -z "$TARGETDIR" ] && [ -e "$SHORTGAMENAME" ] && export TARGETDIR="$PWD/$SHORTGAMENAME"

[ -z "$LANGUAGE" ] && export LANGUAGE="English"

[ -z "$SKIP_DETECTION" ] && export SKIP_DETECTION=0

### functions ###

function die
{
	echo "Install can not continue"

	if [ "$alwaysdiesafely" != "1" ]; then
		echo "Cleaning up partial install"
		rm -rf "$TARGETDIR"
	fi

	echo ""
	echo "The install failed; sorry."
	echo "Please make sure you have the correct CDs for the game"
	echo "and enough free disk space, and try again."

	exit 1
}

function diesoftly
{
	# a non-destructive die

	echo " ! The install failed; sorry." 1>&2

	exit 1
}

function diequietly
{
	exit 1
}

function setuptmp
{
	rm -rf "$TMPDIR"
	mkdir -p "$TMPDIR" || die
}

function cleanuptmp
{
	rm -rf "$TMPDIR"
}

function checkforbin
{
	# usage: checkforbin binnames...
	retstatus=0

	while [ "$1" != "" ]; do
		binname="$1"

		which "$binname" &> /dev/null
		if [ $? -ne 0 ]; then
			echo "WARNING: This script needs $binname, which doesn't appear to be installed." 1>&2
			retstatus=1
		fi

		shift
	done

	return $retstatus
}

function setperms
{
	# usage: setperms targetdir

	if ! [ -d "$1" ]; then
		return 1
	fi

	find "$1" -type d -exec chmod 755 '{}' \;
	find "$1" -type f -exec chmod 644 '{}' \;
	find "$1" -type f -iname '*.exe' -exec chmod 755 '{}' \;
	find "$1" -type f -iname '*.ini' -exec chmod 664 '{}' \;
	chgrp -R games "$1" &> /dev/null
	if [ -d "$1"/save ]; then
		chmod -R g+w "$1"/save
	fi
	if [ -d "$1"/mpsave ]; then
		chmod -R g+w "$1"/mpsave
	fi
}

function usage
{
	echo "Usage:" $0 "[-i installdir] [-p patchdir] [-c cdmount] [-l language] [-n]"
	echo -e "  -i installdir is the directory to install to."
	echo -e "     default: ${TARGETDIR} (changes per game)"
	echo -e "  -c cdmount is the mount location of the cd drive used."
	echo -e "     default: ${CDMOUNT}"
	echo -e "  -p patchdir is an optional directory containing patch files."
	echo -e "     default: ${PATCHDIR}"
	echo -e "  -l language can be one of English, Spanish, French, German, Italian   or Language Independant"
	echo -e "     default: ${LANGUAGE}"
	echo -e "  -n skip CD autodetection"
	echo -e "     default: off"
}

function parseargs
{
	# usage: parseargs args...

	while getopts ":i:p:c:l:n" options; do
		case $options in
			i ) export TARGETDIR="$OPTARG";;
			p ) export PATCHDIR="$OPTARG";;
			c ) export CDMOUNT="${OPTARG%\/}";; # ensure no trailing slash
			l ) export LANGUAGE="$OPTARG";;
			n ) export SKIP_DETECTION=1;;
			\? )	usage
				exit 1;;
			h ) usage
				exit 1;;
		esac
	done
}

function copylower
{
	# usage: copylower source destination
	mkdir -p "$2"

	if [ -d "$1" ]; then
		for filename in $(find "$1" -type f); do
			lowerpath="$(echo $filename|gawk -F $1 '{print $2}'|tr A-Z a-z)"
			cp -f "$filename" "${2}/${lowerpath}" || die
		done
	elif [ -f "$1" ]; then
		lowername="$(basename $1|tr A-Z a-z)"
		cp -f "$1" "${2}/${lowername}"
	fi
}

function setlower
{
    # usage: setlower target
    if [ "$1" ]
    then
        cd "$1"
        for each_file in `find ./ -iname "*"`
        do
            lower="`echo "$each_file" | tr "[:upper:]" "[:lower:]"`"

            if [ "$each_file" != "$lower" ]
            then
                mv "$each_file" "$lower"
            fi
        done
    fi
}

function move_and_remove
{
    # usage: move_and_remove source destination
    # copies the files in the source directory to the destination.
    # then removes the source entirely.
    if [ "$1" != "$2" ]
    then
        mkdir -p "$2" || die
        cp -R "$1/"* "$2" || die
        rm -r "$1"
    fi
}

function teardown
{
    # usage: teardown target
    if [ "$1" ]
    then
        cleanuptmp
        setperms "$1"
        setlower "$1"
    fi
}
