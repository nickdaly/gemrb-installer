# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.

### default variables ###

[ -z "$PATCHDIR" ] && export PATCHDIR="$PWD"

### functions ###

function getpatch
{
	# usage: getpatch url [filename]

	url=$1
	if [ $# -gt 1 ]; then
		filename="$2"
	else
		filename=$(basename $url)
	fi

	if [ -f "$PATCHDIR/$filename" ]; then
		cp "$PATCHDIR/$filename" $TMPDIR || die
	else
		wget $url -O $TMPDIR/$filename || die
	fi
}
