# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.

### default variables ###

[ -z "$CDMOUNT" ] && export CDMOUNT="/media/cdrom"

# the MD5sum of the data1.cab of the first CD in each set.
export BG1_5CD_INTL_MD5="138528155dfeb695bd63f90eacee7dc0"
export BG1_TOTSC_3CD_US_MD5="1a6828b97a27967e8c4acc25e1ef48d2"
export BG1_TOTSC_1CD_UK_MD5="e27d259ddc0171ff945dbba136e60309"

export BG2_4CD_US_MD5="8acf4a6348de916bfaf077469a427b65"
export BG2_TOB_1CD_US_MD5="b1f247a952c13cb8e0b6efc108aaeeaa"

export PST_2CD_MD5="640b61443cc86c434f1b6826e63c0e33"
export PST_4CD_MD5="5e8c2075163aca124de4a467fb33063d"

export IWD_2CD_MD5="b79582f4f80270d48e6b26f529ba31dd"
export IWD_HOW_1CD_US_MD5="d2c42de47026f6384053965319ff511b"

export IWD2_2CD_INTL_MD5="77307366ba3e8a246f6f238cbb3b7874"

# a record of every IE collection I can find.
export IWD_ULTIMATE_COLLECTION_MD5=""
export BG1_1DVD_MD5=""
export BG_COMPLILATION_1DVD_MD5=""
#export BG2_COMPLILATION_NCD_MD5="" # no idea how many cds.
export BLACK_ISLE_COMPILATION_PART_ONE_MD5=""
export BLACK_ISLE_COMPILATION_PART_TWO_MD5=""
export ULTIMATE_DUNGEONS_DRAGONS_MD5=""
export ROLLENSPIELE_DELUXE_EDITION_MD5=""

### functions ###

function checkcd
{
	# usage: checkcd md5sum md5file
	checksum=$(md5sum $2 2>/dev/null|awk '{print $1}')

	if [ "$checksum" != "$1" ]; then
		return 1
	fi

	return 0
}

function getcd
{
	# usage: getcd cdnum [md5sum] [md5file]
	# note we're redirecting to stderr to ensure questions
	# are always shown

	if [ $# -ge 3 ]; then
		answer=""
		while [ $(checkcd $2 $CDMOUNT/$3; echo $?) -ne 0 ]; do
			echo " * Insert CD $1 and press any key, or press 'c' to continue without checking" 1>&2
			read -sn 1 answer
			if [ "$answer" == 'c' ]; then
				return 0
			fi
		done
	else
		echo "* Please insert CD $1 and press any key" 1>&2
		read -sn 1
	fi

	return 0
}
