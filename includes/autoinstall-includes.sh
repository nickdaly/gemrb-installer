# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.

### variables ###

[ -z "$FULLINSTALL" ] && export FULLINSTALL=1

### functions ###

# $1 = question
# $2 = default boolean answer [yYnN]
# $3 = title
# $4 = long explanation
function query
{
    # usage: query question default [title] [longExplanation]

    if [ "$3" ]; then
        echo -e "$3"
        echo "========================================"
        echo
    fi

    if [ "$4" ]; then
        echo -e "$4"
        echo
    fi


	while true; do
		RESPONSE=""
		echo -e -n "$1 [$2] "
		read -n 1 RESPONSE
		echo

		RESPONSE=${RESPONSE:=$2}
		case $RESPONSE in
			n|N) return 1  ;;
			y|Y) return 0  ;;
		esac
	done
}

# a simplified version of query() from sorcery
# $1 = question
# $2 = default boolean answer [yYnN]
# $3 = title
# $4 = long explanation
function new_query
{
    # usage: query question default [title] [longExplanation]

    if [ "$PLAYONLINUX" ]; then
        RESPONSE=query_playOnLinux "$1" "$2" "$3" "$4"
    else
        RESPONSE=query_text "$1" "$2" "$3" "$4"
    fi

    return $RESPONSE
}

# a simplified version of query(), modified for PoL.
# $1 = question
# $2 = default boolean answer [yYnN]
# $3 = title
# $4 = long explanation
function query_playOnLinux
{
    # usage: query_playOnLinux question default [title] [longExplanation]

    if [ "$4" ]; then
        1="$4

$1"
    fi

    POL_SetupWindow_question "$1 [$2]" "$3"
    return "$APP_ANSWER"
}

# a simplified version of query() from sorcery
# $1 = question
# $2 = default boolean answer [yYnN]
# $3 = title
# $4 = long explanation
function query_text
{
    # usage: query_text question default [title] [longExplanation]

    if [ "$3" ]; then
        echo -e "$3"
        echo "========================================"
        echo
    fi

    if [ "$4" ]; then
        echo -e "$4"
        echo
    fi


	while true; do
		RESPONSE=""
		echo -e -n "$1 [$2] "
		read -n 1 RESPONSE
		echo

		RESPONSE=${RESPONSE:=$2}
		case $RESPONSE in
			n|N) return 1  ;;
			y|Y) return 0  ;;
		esac
	done
}

function recommendcfg
{
	# usage: recommendcfg gemrbname [numofcds] [startcd]

	if [ "$2" ]; then
		NUMOFCDS="$2"
	else
		NUMOFCDS=1
	fi

    if [ "$3" ]; then
        STARTCD="$3"
    else
        STARTCD=1
    fi

	gemrbDir="${HOME}/.gemrb"
	installRegistry="${gemrbDir}/installations.ini"
	config="./GemRB.cfg"

	displayText="To play the game with GemRB, you'll need to modify your config file:\n$config"

 	# record the installation in the installation registry.
	mkdir "$gemrbDir" 2>/dev/null
	touch "$installRegistry" 2>/dev/null
	echo "$TARGETDIR" 2>/dev/null >> "$installRegistry"

	# save the GamePath and CD# config file keys
	configData="GameType=$1\n"
	configData="${configData}GamePath=$TARGETDIR\n"
	if [ $FULLINSTALL -eq 1 ]; then
		for CDNUMBER in $(seq $STARTCD $(($NUMOFCDS+$STARTCD-1))); do
            if [ -d "${TARGETDIR}/cd${CDNUMBER}" ]; then
                LINE="CD${CDNUMBER}=${TARGETDIR}/cd${CDNUMBER}"
            else
			    LINE="CD${CDNUMBER}=${TARGETDIR}"
            fi
			configData="${configData}${LINE}\n"
		done
	else
		for CDNUMBER in $(seq $STARTCD $(($NUMOFCDS+$STARTCD-1))); do
			LINE="CD${CDNUMBER}=${CDMOUNT}/cd${CDNUMBER}"
			configData="${configData}${LINE}\n"
		done
	fi

	#  recommend GameOnCD if startcd exists
	if [ $STARTCD -eq 1 ] ; then
		[ $FULLINSTALL -eq 1 ] && configData="${configData}GameOnCD=0\n" || configData="${configData}GameOnCD=1\n"
	fi

	touch "$config" 2>/dev/null
	echo -e "$configData" 2>/dev/null >> "$config"

    if [ "$PLAYONLINUX" ]; then
        POL_SetupWindow_message "$displayText" "Setup Complete!"
    else
        echo -e "$displayText"
    fi
}
