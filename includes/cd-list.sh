#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Selects the appropriate installer(s) for an Infinity Engine game,
# based on the CD inserted

function get_game
{
	while [ -z "$MINSCRIPT" ]; do
		case $CDMD5 in
			$BG1_5CD_INTL_MD5 )
				FULLGAMENAME="Baldurs Gate 1: 5CD International version"
				SHORTGAMENAME="baldursgate1"
				GEMRBNAME="bg1"
				MINSCRIPT="bg1/install-5cd-minimal.sh"
				EXTRASCRIPT="bg1/install-5cd-extra.sh"
				PATCHES="bg1/install-patch-intl.sh bg1/install-patch-baldurdash-fix.sh bg1/install-patch-baldurdash-text.sh"
				WINDOWSFILES="baldur.exe bgmain.exe config.exe luaauto.cfg update.url"
				CDNO=5
				;;
			$BG1_TOTSC_3CD_US_MD5 )
				FULLGAMENAME="Baldurs Gate 1 & Tales of the Sword Coast: 3CD US version"
				SHORTGAMENAME="baldursgate1totsc"
				GEMRBNAME="bg1"
				MINSCRIPT="bg1/install-3cd-minimal.sh"
				EXTRASCRIPT="bg1/install-3cd-extra.sh"
				PATCHES="bg1/install-patch-baldurdash-fix.sh bg1/install-patch-totsc-baldurdash-text.sh"
				WINDOWSFILES="baldur.exe bgmain.exe config.exe luaauto.cfg update.url"
				CDNO=3
				;;
			$BG1_TOTSC_1CD_UK_MD5 )
				FULLGAMENAME="Baldurs Gate 1 & Tales of the Sword Coast: 1CD UK version"
				SHORTGAMENAME="baldursgate1totsc"
				GEMRBNAME="bg1"
				MINSCRIPT="bg1/install-1cd-totsc-uk.sh"
				PATCHES="bg1/install-patch-totsc-uk.sh bg1/install-patch-baldurdash-fix.sh bg1/install-patch-totsc-baldurdash-text.sh"
				WINDOWSFILES="bgmain2.exe baldur.exe config.exe mconvert.exe"
				CDNO=1
				CDSTART=6
				;;
			$BG2_4CD_US_MD5 )
				FULLGAMENAME="Baldurs Gate 2 - Shadows of Amn: 4CD US version"
				SHORTGAMENAME="baldursgate2soa"
				GEMRBNAME="bg2"
				MINSCRIPT="bg2/install-soa-4cd-us-minimal.sh"
				EXTRASCRIPT="bg2/install-soa-4cd-us-extra.sh"
				EXPANSIONSCRIPT="bg2/install-soa-4cd-us-tobMinimal.sh"
				EXPANSIONNAME="Throne of Bhaal"
				PATCHES="" # TODO: fill in patches
				# bg2/install-patch-soa-us.sh bg2/install-patch-baldurdash-fix.sh bg2/install-patch-soa-baldurdash-text.sh"
				WINDOWSFILES="bgdxtest.exe bggltest.exe glsetup.exe bgconfig.exe bgmain.exe charview.exe"
				CDNO=4
				;;
			$BG2_TOB_1CD_US_MD5 )
				FULLGAMENAME="Baldurs Gate 2 - Throne of Bhaal: 1CD US version"
				SHORTGAMENAME="baldursgate2soa"
				GEMRBNAME="tob"
				MINSCRIPT="bg2/install-tob-1cd-us-minimal.sh"
				PATCHES="bg2/install-tob-patch-us.sh"
				# "bg1/install-patch-baldurdash-fix.sh bg1/install-patch-totsc-baldurdash-text.sh"
				WINDOWSFILES="autorun.ini bgconfig.exe bgdxtest.exe bggltest.exe bgmain.exe charview.exe glsetup.exe update.url earthlink gamespy"
				CDNO=1
				CDSTART=5
				;;
			$PST_2CD_MD5 )
				FULLGAMENAME="Planescape Torment: 2CD version"
				SHORTGAMENAME="planescapetorment"
				GEMRBNAME="pst"
				MINSCRIPT="pst/install-2cd-minimal.sh"
				EXTRASCRIPT="pst/install-2cd-extra.sh"
				WINDOWSFILES="torment.exe ereg"
				CDNO=2
				;;
			$PST_4CD_MD5 )
				FULLGAMENAME="Planescape Torment: 4CD version"
				SHORTGAMENAME="planescapetorment"
				GEMRBNAME="pst"
				MINSCRIPT="pst/install-4cd-minimal.sh"
				EXTRASCRIPT="pst/install-4cd-extra.sh"
				PATCHES="pst/install-patch-official.sh"
				WINDOWSFILES="torment.exe ereg"
				CDNO=4
				;;
			$IWD_2CD_MD5 )
				FULLGAMENAME="Icewind Dale: 2CD version"
				SHORTGAMENAME="icewinddale"
				GEMRBNAME="iwd"
				MINSCRIPT="iwd/install-2cd-minimal.sh"
				EXTRASCRIPT="iwd/install-2cd-extra.sh"
				PATCHES="iwd/install-patch-intl.sh iwd/install-patch-uk-fix.sh"
				WINDOWSFILES="3dfx.dll config.exe icewind.ini language.ini mpicewnd.mpi"
				CDNO=2
				;;
			$IWD2_2CD_INTL_MD5 )
				FULLGAMENAME="Icewind Dale 2: 2CD US version"
				SHORTGAMENAME="icewinddale2"
				GEMRBNAME="iwd2"
				MINSCRIPT="iwd2/install-2cd-us-minimal.sh"
				EXTRASCRIPT="iwd2/install-2cd-us-extra.sh"
				PATCHES="iwd2/install-patch-us.sh"
				WINDOWSFILES="3dfx.dll config.exe icewind.ini language.ini mpicewnd.mpi"
				CDNO=2
				;;
			* )
				[ $SKIP_DETECTION -eq 0 ] && \
					echo "Unrecognised CD - please select game from menu or change the disc in ${CDMOUNT}"
				choosegame
				;;
		esac
	done
	
	export FULLGAMENAME SHORTGAMENAME MINSCRIPT EXTRASCRIPT EXPANSIONSCRIPT EXPANSIONNAME PATCHES WINDOWSFILES CDNO CDSTART GEMRBNAME
}

function choosegame
{
	while true; do
		choice=0
        # TODO: don't forget to add all the games to this list.
		echo "Choose the game to install from the list:"
		echo "1  | Baldur's Gate 1 (5CD International version)"
		echo "2  | Baldur's Gate & Tales of the Sword Coast (3CD US version)"
		echo "3  | Tales of the Sword Coast (1CD UK version)"
		echo "4  | Baldur's Gate 2 - Shadows of Amn (4CD US version)"
		echo "5  | Baldur's Gate 2 - Throne of Bhaal (1CD US version)"
		echo "6  | Planescape: Torment (2CD version)"
		echo "7  | Planescape: Torment (4CD version)"
		echo "8  | Icewind Dale (2CD UK version)"
		echo "9  | Icewind Dale 2 (2CD International version)"
		echo "Z  | My game isn't listed."
		echo "Q  | Quit"

		read -n 1 choice
		echo

		case $choice in
			1 ) export CDMD5=$BG1_5CD_INTL_MD5
				return 0 ;;
			2 ) export CDMD5=$BG1_TOTSC_3CD_US_MD5
				return 0 ;;
			3 ) export CDMD5=$BG1_TOTSC_1CD_UK_MD5
				return 0 ;;
			4 ) export CDMD5=$BG2_4CD_US_MD5
				return 0 ;;
			5 ) export CDMD5=$BG2_TOB_1CD_US_MD5
				return 0 ;;
			6 ) export CDMD5=$PST_2CD_MD5
				return 0 ;;
			7 ) export CDMD5=$PST_4CD_MD5
				return 0 ;;
			8 ) export CDMD5=$IWD_2CD_UK_MD5
				return 0 ;;
			9 ) export CDMD5=$IWD2_2CD_INTL_MD5
				return 0 ;;
			[Qq] ) diequietly
				return 0 ;;
			[Zz] ) echo
				"$(dirname $0)/disc_collector.sh"
				return 0 ;;
		esac
	done
}
