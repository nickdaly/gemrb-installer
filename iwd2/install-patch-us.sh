#!/bin/bash
# Copyright 2009 Tasos Latsas
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Icewind Dale 2 US version patch v2.01
#
# info : http://www.planetbaldursgate.com/iwd2/media/files/
# size : 4.95 MB

patch_url="http://www.sorcerers.net/Games2/IWD2/IWD2Patch201.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/patch-includes.sh"

parseargs "$@"

checkforbin cabextract wget unshield

setuptmp

getpatch $patch_url

cabextract -L "${TMPDIR}/${patch_name}" -d "$TMPDIR" || diesoftly
# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TMPDIR" -L x "${TMPDIR}/disk1/data1.cab" || diesoftly
cd "$TMPDIR" && unshield -d . -L x "${TMPDIR}/disk1/data1.cab" || die

cp -R "${TMPDIR}/minimum_compressed_-_language_independent/override/"* "${TARGETDIR}"/override || diesoftly
cp -R "${TMPDIR}/minimum_compressed_-_language_independent/scripts/"* "${TARGETDIR}"/scripts || diesoftly

cp -R "${TMPDIR}/minimum_compressed_-_us_english/characters/"* "${TARGETDIR}/characters" || diesoftly
cp -f "${TMPDIR}/minimum_compressed_-_us_english/dialog.tlk" "$TARGETDIR" || diesoftly

cleanuptmp
setperms "$TARGETDIR"
