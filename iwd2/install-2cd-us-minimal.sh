#!/bin/bash
# Copyright 2009 Tasos Latsas
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Icewind Dale 2 (2CD US version) minimal

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

getcd 1 $IWD2_2CD_US_MD5 data1.cab

mkdir -p "$TARGETDIR" || die

# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TARGETDIR" -L x "${CDMOUNT}/data1.cab" || die
cd "$TARGETDIR" && unshield -d . -L x "${CDMOUNT}/data1.cab" || die

move_and_remove "${TARGETDIR}/minimum_compressed_-_us_english/" "$TARGETDIR" || die
move_and_remove "${TARGETDIR}/minimum_compressed_-_language_independent/" "$TARGETDIR" || die

copylower "${CDMOUNT}/Data/" "${TARGETDIR}/data" || die
copylower "${CDMOUNT}/Icewind2.ico" "$TARGETDIR" || die

rm -r "${TARGETDIR}/"_*
setperms "$TARGETDIR"
