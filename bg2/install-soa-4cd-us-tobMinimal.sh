#! /bin/bash
#
# Baldur's Gate 2 Shadows of Amn (4CD US version) Minimal TOB Install
# Copyright (C) 2009 GemRB Installers, Nick Daly
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

CD2_MD5="c78f41ff4cb47ea04fdf25ea6f55eb8b"

getcd 2 $CD2_MD5 "data3.cab"
copylower "${CDMOUNT}/CD2/" "$TARGETDIR/cd2/data"

teardown "$TARGETDIR"
