#!/bin/bash
#
# Baldur's Gate 2 Shadows of Amn (4CD US version) Minimal Install
# Copyright (C) 2009 GemRB Installers, Nick Daly
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA    
# 02110-1301 USA.

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

getcd 1 $BG2_4CD_US_MD5 data1.cab

mkdir -p "$TARGETDIR" || die
mkdir -p "${TARGETDIR}/portraits" || die

cp "${CDMOUNT}/baldur.ico" "$TARGETDIR" || die

# work around unshield bug (#2801016 in SynCE project)
#unshield -L x "${CDMOUNT}/data1.cab" -d "$TARGETDIR" || die
cd "$TARGETDIR" && unshield -d . -L x "${CDMOUNT}/data1.cab" || die

rm -r "${TARGETDIR}/_"* # remove all the the fake directories.

for i in "${TARGETDIR}/hd0_"* ; do j=`echo $i | sed 's#hd0_##g' - ` ; mv "$i" "$j" ; done

move_and_remove "${TARGETDIR}/cab" "${TARGETDIR}"
move_and_remove "${TARGETDIR}/compiler" "${TARGETDIR}/script compiler"
move_and_remove "${TARGETDIR}/sound" "${TARGETDIR}/sounds"

for directory in "compiled" "decompiled" "errors"; do mkdir -p "${TARGETDIR}/script compiler/${directory}" || die; done

rm -r "${TARGETDIR}/cache100" "${TARGETDIR}/cache/cache150.txt" "${TARGETDIR}/mplayer" "${TARGETDIR}/register"

teardown "$TARGETDIR"
