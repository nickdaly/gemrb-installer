#!/bin/bash
#
# Baldur's Gate 2 Throne of Bhaal (1CD US version) Minimal Install
# Copyright (C) 2009 GemRB Installers, Nick Daly
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA    
# 02110-1301 USA.

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

getcd 1 $BG2_TOB_1CD_US_MD5 data1.cab

mkdir -p "$TARGETDIR" || die

if [ ! -e "${TARGETDIR}/cd2/data/area000a.bif" ]
then
    echo "You haven't installed BG2-SOA!"
    die
    # TODO ask user to run the appropriate preinstaller.
fi

# work around unshield bug (#2801016 in SynCE project)
#unshield -d "$TARGETDIR" -L x "${CDMOUNT}/data1.cab" || die
cd "$TARGETDIR" && unshield -d . -L x "${CDMOUNT}/data1.cab" || die

rm -r "${TARGETDIR}/_"* # remove all the the fake directories.

move_and_remove "${TARGETDIR}/hd025music" "${TARGETDIR}/music"
move_and_remove "${TARGETDIR}/hd0_english/characters" "${TARGETDIR}/characters"
move_and_remove "${TARGETDIR}/hd0_english/override" "${TARGETDIR}/override"
move_and_remove "${TARGETDIR}/hd0_override" "${TARGETDIR}/override"
move_and_remove "${TARGETDIR}/minimum_25_data" "${TARGETDIR}/data"
move_and_remove "${TARGETDIR}/minimum_25_data_english" "${TARGETDIR}/data"
move_and_remove "${TARGETDIR}/hd0_english/" "${TARGETDIR}/"
move_and_remove "${TARGETDIR}/hd0" "${TARGETDIR}/"

teardown "$TARGETDIR"
