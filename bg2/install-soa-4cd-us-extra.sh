#! /bin/bash
#
# Baldur's Gate 2 (4CD US version) Full Install
# Copyright (C) 2009 GemRB Installers, Nick Daly
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.

scriptdir="$(dirname $0)"/..
. "${scriptdir}/includes/generic-includes.sh"
. "${scriptdir}/includes/cd-includes.sh"

parseargs "$@"

checkforbin unshield

CD2_MD5="c78f41ff4cb47ea04fdf25ea6f55eb8b"
CD3_MD5="61fa51451c9ab102383c082fac3687dc"
CD4_MD5="d13f925c01c1f78fd35baa708715ebbd"

function bgtwo_smart_install
{
    # usage cd#
    cp -r "${CDMOUNT}/CD${1}/" "$TARGETDIR/data"
    chmod -R 755 "$TARGETDIR/data/CD${1}"
    mkdir -p "$TARGETDIR/cd${1}/data"
    mv "$TARGETDIR/data/CD${1}/Data/AREA"*".bif" "$TARGETDIR/cd${1}/data"
    mv "$TARGETDIR/data/CD${1}/Data/"* "$TARGETDIR/data"
    mv "$TARGETDIR/data/CD${1}/Movies" "$TARGETDIR/cd${1}/movies"
    rm -r "$TARGETDIR/data/CD${1}/"

}

getcd 2 $CD2_MD5 "data3.cab"
bgtwo_smart_install 2

getcd 3 $CD3_MD5 "data4.cab"
bgtwo_smart_install 3

getcd 4 $CD4_MD5 "data5.cab"
bgtwo_smart_install 4

teardown "$TARGETDIR"
