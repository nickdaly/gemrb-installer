#!/bin/bash
# Copyright 2010 Tasos Latsas, Nick Daly
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Baldur's Gate 2 Throne of Bhaal Patch
#
# info : http://www.bioware.com/games/throne_bhaal/support/patches/
# size : 8.0 MB
# date : before 12/27/2001 (earliest archive.org version)

patch_url="http://downloads.bioware.com/baldursgate2/BGII-ThroneofBhaal_Patch_26498_EUROPEAN.exe"
patch_name="$(basename $patch_url)"

scriptdir="$(dirname $0)"/..
. "$scriptdir/includes/generic-includes.sh"
. "$scriptdir/includes/patch-includes.sh"

parseargs "$@"

checkforbin cabextract wget

setuptmp

getpatch $patch_url

cabextract -L -s "${TMPDIR}/${patch_name}" -d "$TMPDIR" || diesoftly
unshield x -Ld "${TMPDIR}/data1.cab" -d "${TMPDIR}/data1"
cp "${TMPDIR}/data1/override/"*.exe "${TARGETDIR}" || diesoftly
cp -R "${TMPDIR}/data1/override/"* "${TARGETDIR}/override" || diesoftly

teardown "$TARGETDIR"
