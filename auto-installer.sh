#!/bin/bash
# Copyright 2009 Nick White
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See <http://www.gnu.org/licenses/> for a copy of the GNU General
# Public License.
#
# Runs the appropriate installer(s) for an Infinity Engine game,
# based on the CD inserted

FULLGAMENAME=''
MINSCRIPT=''
EXTRASCRIPT=''
PATCHES=''
CDNO=''
CDSTART=''

scriptdir="$(dirname $0)"
. "$scriptdir/includes/generic-includes.sh"
. "$scriptdir/includes/cd-includes.sh"
. "$scriptdir/includes/patch-includes.sh"
. "$scriptdir/includes/autoinstall-includes.sh"
. "$scriptdir/includes/cd-list.sh"
if [ "$PLAYONLINUX" ]; then
    . "$PLAYONLINUX/lib/sources" > /dev/null 2>&1
fi

parseargs "$@"

echo "Checking CD version"

if [ $SKIP_DETECTION -eq 0 ]; then
	export CDMD5=$(md5sum "${CDMOUNT}/data1.cab"|awk '{print $1}')
else
	export CDMD5=""
fi

get_game

[ -z "$TARGETDIR" ] && [ -n "$SHORTGAMENAME" ] && export TARGETDIR="$PWD/$SHORTGAMENAME"
[ -z "$TARGETDIR" ] && diequietly

# if targetdir already exists, make sure we never clobber it
[ -d "$TARGETDIR" ] && export alwaysdiesafely=1

echo "$FULLGAMENAME detected, installing"
"${scriptdir}/${MINSCRIPT}" -i "$TARGETDIR" -c "$CDMOUNT" 1>/dev/null || diequietly

if [ -n "$EXTRASCRIPT" ]; then
	if query "Do you want to do a full install, so the CDs will not be needed again?" y; then
		FULLINSTALL=1
		"${scriptdir}/${EXTRASCRIPT}" -i "$TARGETDIR" -c "$CDMOUNT" 1>/dev/null || diequietly
	else
		FULLINSTALL=0
	fi
fi

if [ -n "$EXPANSIONSCRIPT" ]; then
    if query "Do you want to install the files needed by the $EXPANSIONNAME expansion?" y; then
	    EXPANSIONINSTALL=1
	    "${scriptdir}/${EXPANSIONSCRIPT}" -i "$TARGETDIR" -c "$CDMOUNT" 1>/dev/null || diequietly
    else
	    EXPANSIONINSTALL=0
    fi
fi

if [ -n "$PATCHES" ]; then
	for patch in $PATCHES; do
		patch="$scriptdir/$patch"
		# grab 17th line from scripts (description line; after license)
		patchtitle=$(sed -n 17p "$patch"|cut -d ' ' -f 2-)
		if query "Do you want to install $patchtitle" y; then
			"$patch" \
			-i "$TARGETDIR" \
			-p "$PATCHDIR" \
			-l "$LANGUAGE" \
			-c "$CDMOUNT" \
			1>/dev/null
		fi
	done
fi

if [ -n "$WINDOWSFILES" ]; then
	if query "Do you want to remove Windows-only files (may break compatibility with some mods)?" n; then
		for badfile in $WINDOWSFILES; do
			rm -rf "${TARGETDIR}/${badfile}"
		done
	fi
fi

echo
echo "$FULLGAMENAME installed successfully"

recommendcfg "$GEMRBNAME" "$CDNO" "$CDSTART"
